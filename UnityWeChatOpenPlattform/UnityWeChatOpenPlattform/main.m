//
//  main.m
//  UnityWeChatOpenPlattform
//
//  Created by yongchao wang on 2017/5/8.
//  Copyright © 2017年 yongchao wang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
